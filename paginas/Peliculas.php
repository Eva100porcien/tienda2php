<?php
//Me creo una plantilla llamada Pelicula
class Pelicula{
	//Propiedades (Variables)
	public $titulo;
	public $director;
	public $genero;
	public $actores;
	

	//Metodos
	function __construct($titulo, $genero, $director){
		$this->titulo=$titulo;
		$this->genero=$genero;
		$this->director=$director;
		$this->actores=array();
		
	}

	function dimeInfo(){
		return "<p>$this->titulo, $this->genero, $this->director </p>" ;

	}

	function addActor($actor){
		$this->actores[]=$actor;
	}

	function dimeActores(){
		$resultado='';
		for ($i=0; $i < count($this->actores); $i++){
			$resultado.=$this->actores[$i];
			$resultado.='<br>';
		}
		return $resultado;
	}

}


$pel=new Pelicula('Alien Covenant', 'Ridley Scott', 'Terror');
$pel->addActor('Magneto');
$pel->addActor('el que muere primero');


echo $pel->dimeInfo();
echo $pel->dimeActores();