<br>
<h4>Modificar producto</h4>

<?php  
$id=$_GET['id'];
$sql="SELECT * FROM TablaEnlaces WHERE idEn=$id";
$consulta=mysqli_query($conexion, $sql);
$r=mysqli_fetch_array($consulta);
?>

  
  

  <div class="form-group">
    <label for="idCat">Categoria del producto</label>
    <select class="form-control" id="idCat" name="idCat">
      <?php
      $sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
      $consultaCat=mysqli_query($conexion, $sqlCat);
      while($rCat=mysqli_fetch_array($consultaCat)){
        if($rCat['idCat']==$r['idCat']){
          $sel='selected';
        }else{
          $sel='';
        }

        ?>
        <option value="<?php echo $rCat['idCat'];?>" <?php echo $sel;?>>
          <?php echo $rCat['nombreCat'];?>
        </option>
        <?php
      }
      ?>
    </select>
  </div>

  <div class="form-group">
    <input type="submit" class="form-control" name="insertar" value="Guardar producto">
  </div>

  <input type="hidden" name="idProd" value="<?php echo $id;?>">

</form>
